package main

import (
	"flag"
	"fmt"
	"os"

	"github.com/richardlehane/crock32"
)

var (
	encode = flag.Uint64("e", 0, "uint64 to encode")
	decode = flag.String("d", "", "string to decode")
)

func main() {
	flag.Parse()
	if *encode != 0 {
		fmt.Println(crock32.Encode(*encode))
		return
	}
	if *decode != "" {
		value, err := crock32.Decode(*decode)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Can't decode %#v: %v", *decode, err)
			os.Exit(1)
		}
		fmt.Println(value)
		return
	}

	fmt.Fprintln(os.Stderr, "You need to specify if you want to decode or encode")
	flag.PrintDefaults()
	os.Exit(1)
}
